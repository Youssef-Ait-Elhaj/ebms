package com.emsi.bms.repository;

import com.emsi.bms.entity.ERole;
import com.emsi.bms.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByRoleName(ERole name);
}
