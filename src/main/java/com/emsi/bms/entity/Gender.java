package com.emsi.bms.entity;

public enum Gender {
    MALE,
    FEMALE
}
