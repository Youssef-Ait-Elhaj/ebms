package com.emsi.bms.entity;

public enum TransactionType {
    DEPOSIT,
    WITHDRAWAL,
    FUND_TRANSFER,
    ONLINE_PAYMENT,
}
