package com.emsi.bms.service.bank;

import com.emsi.bms.entity.Bank;
import com.emsi.bms.repository.BankRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class BankServiceImpl implements IBankService {

    private BankRepository bankRepository;

    public BankServiceImpl(BankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }

    @Override
    public List<Bank> getAllBanks() {
        return bankRepository.findAll();
    }

    @Override
    public Bank addBank(Bank bank) {
        return bankRepository.save(bank);
    }

    @Override
    public Bank getBankById(Long bankId) {
        return bankRepository.findById(bankId).get();
    }

    @Override
    public Bank updateBank(Long bankId, Bank newBank) {
        newBank.setId(bankId);
        return bankRepository.save(newBank);
    }

    @Override
    public void deleteBankById(Long bankId) {
        bankRepository.deleteById(bankId);
    }
}
