package com.emsi.bms.service.account;

import com.emsi.bms.entity.Account;
import com.emsi.bms.entity.Transaction;
import com.emsi.bms.repository.AccountRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AccountService implements IAccountService {

    private AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }

    @Override
    public Account getAccountById(Long accountId) {
        return accountRepository.findById(accountId).get();
    }

    @Override
    public void deleteAccount(Long accountId) {
        accountRepository.deleteById(accountId);
    }

    @Override
    public List<Transaction> getAccountTransactions(Long accountId) {
        return accountRepository.findById(accountId).get().getTransactionCollection();
    }
}
