package com.emsi.bms.web;

import com.emsi.bms.entity.User;
import com.emsi.bms.service.customer.IUserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CustomerRestController {

    private IUserService customerService;

    public CustomerRestController(IUserService customerService) {
        this.customerService = customerService;
    }

    @PostMapping("/customers")
    public User addCustomer(@RequestBody User user) {
        return customerService.addUser(user);
    }

    @GetMapping("/customers")
    public List<User> getAllCustomers() {
        return customerService.getAllUsers();
    }

    @GetMapping("/customers/{id}")
    public User getCustomerById(@PathVariable("id") Long customerId) {
        return customerService.getUserById(customerId);
    }

    @DeleteMapping("/customers/{id}")
    public void deleteCustomer(@PathVariable("id") Long customerId) {
        customerService.deleteUserById(customerId);
    }
}
