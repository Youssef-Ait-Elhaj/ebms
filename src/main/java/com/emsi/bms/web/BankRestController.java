package com.emsi.bms.web;

import com.emsi.bms.entity.Bank;
import com.emsi.bms.service.bank.IBankService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class BankRestController {

    private IBankService bankService;

    public BankRestController(IBankService bankService) {
        this.bankService = bankService;
    }

    @PostMapping("/banks")
    public Bank addBank(@RequestBody Bank bank) {
        return bankService.addBank(bank);
    }

    @GetMapping("/banks")
    public List<Bank> getAllBanks() {
        return bankService.getAllBanks();
    }

    @GetMapping("/banks/{id}")
    public Bank getBankById(@PathVariable("id") Long bankId) {
        return bankService.getBankById(bankId);
    }

    @PutMapping("/banks/{id}")
    public Bank updateBank(@PathVariable("id") Long bankId, @RequestBody Bank newBank) {
        return this.bankService.updateBank(bankId, newBank);
    }

    @DeleteMapping("/banks/{id}")
    public void deleteCustomer(@PathVariable("id") Long bankId) {
        bankService.deleteBankById(bankId);
    }
}
