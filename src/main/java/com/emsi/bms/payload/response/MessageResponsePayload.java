package com.emsi.bms.payload.response;

public class MessageResponsePayload {
    private String message;

    public MessageResponsePayload(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
